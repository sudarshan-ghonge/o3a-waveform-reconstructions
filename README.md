This repo contains the whitened time domain LALInference and BayesWave reconstructions that were used as part of the O3a Catalog paper, as well as the 
power spectral densities (PSD) that were used in whitening the waveforms. The square root of the PSD is the called amplitude spectral density (ASD).

The whitened waveform is obtained by dividing the raw detector strain waveform by the ASD in the frequency domain and converting it back into the time domain.

## LALInference 
The lalinference files contain data in a column-wise format. The first column is the time, the second column is the whitened Max Likelihood waveform etc. 
The files have column names and can be loaded in numpy using `numpy.genfromtxt('filename.dat', names=True)`. 
The Column structure is as follows
###  Time | whitened_ML | whitened_lower_bound_90 | whitened_MAP | whitened_median | whitened_upper_bound_90 ML 

Please note that the time column has the *GPS* time of the data. The next 5 columns are the whitened max likelihood, whitened lower 90 % credible bound, 
whitened max a Posteriori, whitened median, whitened upper 90% credible bound respectively. The whitening procedure here uses the same same PSD as was used in the 
LALinference run. Their units are the standard devitations from noise. 



## BayesWave 
The bayeswave files contain data in column wise format as well. The first column is the time in *seconds from start time of the segment*. That is, 
it varies from 0 to seglen where seglen is the segment length used in the BayesWave analysis. 
To superimpose this plot with the lalinference plots, the time array needs to be added with the value of the first element of LI time column
which is the t=0 time for BayesWave. 
The Column structure is as follows
  
### Time | median | upper50 | lower50 | upper90 | lower90



## PSD

The median PSD of the detector data inferred using the BayesLine algorithm. The column structure is as follows

###  Frequency | Power 
